import { Article } from 'src/app/shared/models/article.model';

export const articles: Article[] = [
  {
    name: 'How to Design a Program Repair Bot : Insights from the Repairnator Project',
    year: 2018,
    abstract: 'Program repair research has made tremendous progress over thelast few years, and software development '    +
    'bots are now being in-vented to help developers gain productivity. In this paper, we inves-tigate the concept of a ' +
    '“program repair bot” and present Repairnator.The Repairnator bot is an autonomous agent that constantly moni-tors '  +
    'test failures, reproduces bugs, and runs program repair toolsagainst each reproduced bug. If a patch is found, '     +
    'Repairnator botreports it to the developers. At the time of writing, Repairnatoruses three different program repair' +
    ' systems and has been operatingsince February 2017. In total, it has studied 11 523 test failures over1 609 '        +
    'open-source software projects hosted on GitHub, and has gen-erated patches for 15 different bugs. Over months, we '  +
    'hit a numberof hard technical challenges and had to make various design andengineering decisions. This gives us a '  +
    'unique experience in thisarea. In this paper, we reflect upon Repairnator in order to sharethis knowledge with the ' +
    'automatic program repair community.',
    url: 'https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/stamp/stamp.jsp?tp=&arnumber=8449240'
  },
  {
    name: 'Why and How Java Developers Break APIs',
    year: 2018,
    abstract: 'Modern software development depends on APIs to reuse code and increase productivity. As most software ' +
    'systems, these libraries and frameworks also evolve, which may break existing clients. However, the main reasons' +
    ' to introduce breaking changes in APIs are unclear. Therefore, in this paper, we report the results of an almost' +
    ' 4-month long field study with the developers of 400 popular Java libraries and frameworks. We configured an '    +
    'infrastructure to observe all changes in these libraries and to detect breaking changes shortly after their '     +
    'introduction in the code. After identifying breaking changes, we asked the developers to explain the reasons '    +
    'behind their decision to change the APIs. During the study, we identified 59 breaking changes, confirmed by the'  +
    ' developers of 19 projects. By analyzing the developers\' answers, we report that breaking changes are mostly '   +
    'motivated by the need to implement new features, by the desire to make the APIs simpler and with fewer elements,' +
    ' and to improve maintainability. We conclude by providing suggestions to language designers, tool builders, '     +
    'software engineering researchers and API developers. ',
    url: 'https://arxiv.org/abs/1801.05198'
  },
  {
    name: 'Semantics-based program verifiers for all languages',
    year: 2016,
    abstract: 'We present a language-independent verification framework that can be instantiated with an operational ' +
    'semantics to automatically generate a program verifier. The framework treats both the operational semantics and ' +
    'the program correctness specifications as reachability rules between matching logic patterns, and uses the sound' +
    ' and relatively complete reachability logic proof system to prove the specifications using the semantics. We '    +
    'instantiate the framework with the semantics of one academic language, KernelC, as well as with three recent '    +
    'semantics of real-world languages, C, Java, and JavaScript, developed independently of our verification '         +
    'infrastructure. We evaluate our approach empirically and show that the generated program verifiers can check '    +
    'automatically the full functional correctness of challenging heap-manipulating programs implementing operations ' +
    'on list and tree data structures, like AVL trees. This is the first approach that can turn the operational '      +
    'semantics of real-world languages into correct-by-construction automatic verifiers. ',
    url: 'https://dl.acm.org/citation.cfm?doid=2983990.2984027'
  },
  {
    name: 'From a Monolith to a Microservices Architecture: An Approach Based on Transactional Contexts',
    year: 2019,
    abstract: 'Microservices have become the software architecture of choice for business applications. Initially '     +
    'originated at Netflix and Amazon, they result from the need to partition, both, software development teams and'    +
    ' executing components, to, respectively, foster agile development and horizontal scalability. Currently, there'    +
    ' is a large number of monolith applications that are being migrated to a microservices architecture. This article' +
    ' proposes the identification of business applications transactional contexts for the design of microservices. '    +
    'Therefore, the emphasis is to drive the aggregation of domain entities by the transactional contexts where they'   +
    ' are executed, instead of by their domain structural inter-relationships. Additionally, we propose a complete '    +
    'workflow for the identification of microservices together with a set of tools that assist the developers on this'  +
    ' process. The comparison of our approach with another software architecture tool and with an expert decomposition' +
    ' in two case studies revealed high precision values, which reflects that accurate service candidates are produced' +
    ', while providing visualization perspectives facilitates the analysis of the impact of the decomposition on the '  +
    'application business logic.',
    url: 'https://link.springer.com/chapter/10.1007%2F978-3-030-29983-5_3'
  },
  {
    name: 'Test-Driven Code Review: An Empirical Study',
    year: 2019,
    abstract: 'Test-Driven Code Review (TDR) is a code review practice in which a reviewer inspects a patch by examining' +
    ' the changed test code before the changed production code. Although this practice has been mentioned positively by ' +
    'practitioners in informal literature and interviews, there is no systematic knowledge of its effects, prevalence, '  +
    'problems, and advantages. In this paper, we aim at empirically understanding whether this practice has an effect on' +
    ' code review effectiveness and how developers\' perceive TDR. We conduct a controlled experiment with 93 developers' +
    ' that perform more than 150 reviews, and 9 semi-structured interviews and a survey with 103 respondents to gather '  +
    'information on how TDR is perceived. Key results from the experiment show that developers adopting TDR find the '    +
    'same proportion of defects in production code, but more in test code, at the expenses of fewer maintainability '     +
    'issues in production code. Furthermore, we found that most developers prefer to review production code as they deem' +
    ' it more critical and tests should follow from it. Moreover, general poor test code quality and no tool support '    +
    'hinder the adoption of TDR. ',
    url: 'Test-Driven Code Review: An Empirical Study'
  },
  {
    name: 'Expressions of Sentiments During Code Reviews:Male vs. Female',
    year: 2016,
    abstract: 'As most of the software developmentorganizations are male-dominated, female developers encounter-ing ' +
    'various negative workplace experiences reported feeling likethey “do not belong”. Exposures to  discriminatory ' +
    'expletivesor negative critiques from their male colleagues may furtherexacerbate those feelings.',
    url: 'https://arxiv.org/pdf/1812.05560.pdf'
  }
];

import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

/**
 * Composant de l'accueil
 */
export class ArticlesListComponent implements OnInit {

  public articles;

  // Les constructeurs de composants sont par conventions toujours vide
  // En revanche on peut passer des paramètres au constructeur
  constructor() {
  }

  // On utilise la méthode ngOnInit pour initialiser le composant.
  // On peut utiliser ngOnDestroy si besoin à la suppression du composant
  ngOnInit(): void {
    // A completer
  }
}

# Ifi

## Auteurs

* Sandro Grébant
* Ahouefa Kpossou
* Zoé Canoen

## Installation d'Angular

Angular fait partie de l'environnement NodeJs et s'installe avec la commande :

    npm install -g @angular/cli
    cd ifi-2019-tp-angular/tp
    npm install

## Génération initiale du projet

**A NE PAS FAIRE POUR LE TP**

Ce projet a été généré par les commandes :

    npm install -g @angular/cli
    ng new my-app
    cd my-app
    ng serve --open

Si vous souhaitez travailler chez vous, vous pourrez refaire ces commandes. Ici le projet est déjà généré.

## Serveur de développement

Pour lancer le serveur, faites `npm start`. Aller sur [http://localhost:4200/](http://localhost:4200/). L'application va se recharger automatiquement à chaque sauvegarde de fichier.

## Génération automatique de code

Lancez `ng generate component component-name` pour générer un nouveau composant.
Vous pouvez aussi utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Questions

Le but du tp est de créer un site de news.
Pour cela vous aurez à votre disposition des données contenues dans le fichier `src/app/mocks/articles.ts`.

#### Accueil
![GitHub Logo](images/Accueil.PNG)

#### Résumé
![GitHub Logo](images/Abstract.PNG)

#### Détail
![GitHub Logo](images/Detail.PNG)

#### Filtre
![GitHub Logo](images/Filter.PNG)


Angular est un langage front, c'est pourquoi nous allons simuler un back.
Si vous cherchez dans le code, vous trouverez parfois du code qui vous expliquera comment faire si nous avions un back. (ref : src/app/shared/services/articles.service.ts).

## L'accueil

Pour l'accueil, nous allons d'abord afficher la liste des articles dans le composant `src\app\components\articles-list`.

Pour cela il faut importer la liste des articles et sauvegarder les articles dans le fichier `src\app\components\articles-list\articles-list.component.ts`.

Une liste d'article est mise à votre disposition dans `src/app/shared/mocks/articles`.

Dans la page html `articles-list.component.html` nous vous demanderons pour commencer d'afficher les noms des articles.

**cf : diapo page 19**

**cf : diapo page 25**


Remplacer ce code dans les fichiers
*src\app\components\articles-list\articles-list.component.ts*
```ts
// import de la liste des articles
import { articles } from 'src/app/shared/mocks/articles';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

export class ArticlesListComponent implements OnInit {

// Attribut articles
public articles;

// Les constructeurs de composants sont toujours vide
constructor() { }

// Création du composant
ngOnInit() {
  // On sauvegarde la resource du mock dans le composant
  this.articles = articles;
}
}
```

Ensuite on utilise la boucle for pour afficher tous les noms des articles

*src\app\components\articles-list\articles-list.component.html*
```html
<div class="row ">
    <div  class="col-12 mat-card article" *ngFor="let article of articles;">
      {{article.name}}
    </div>
</div>
```


## Les Classes

Pour être sûr que l'on récupère bien une liste d'Article nous allons créer une classe dans `src/app/shared/models/article.model.ts`,
et on type l'attribut du fichier `src\app\components\articles-list\articles-list.component.ts`.

**cf : diapo page 27**

On type l'attribut

*src\app\components\articles-list\articles-list.component.ts*
```ts
public articles: Article[];
```

Et on n'oublie pas d'importer la ressource.

*src\app\components\articles-list\articles-list.component.ts*
```ts
import { Article } from 'src/app/shared/models/article.model';
```

## Création d'un composant

Création d'un composant. Faites `ng generate component components/header`.
Cela va créer un dossier `components/header` contenant un fichier html, css, ts et spec.ts (pour les tests).

**cf : diapo page 39**

Note : Un dossier `/styles` se trouve à la racine du projet, vous y trouverez peut être des styles qui vous aiderons à faire la même mise en page que sur le tp.

Vérifiez bien que les routes soient correctes dans le fichier `src/app/app.module.ts` contiennent la bonne route.

**cf : diapo page 17**

## Le header

Une fois le composant header créé, à l'aide de material.angular nous allons créer une Toolbar dans le `header.component.html`.

https://material.angular.io/components/toolbar/overview

Sur Material.angular vous pouvez lire le code source.

![GitHub Logo](images/material-tuto-1.PNG)

Sur Material.angular, vous avez également la possibilité de voir les imports à faire ainsi que des exemples.

![GitHub Logo](images/material-tuto-2.PNG)

Nous allons également créer un filtre dans le header que nous configurerons à la fin.

[https://material.angular.io/components/input/overview](https://material.angular.io/components/input/overview)

**cf : diapo page 15**

## L'abstract déroulant

Créons un nouveau composant, nous appelerons ce composant `articles-list-abstract`.

Cela sera le résumé de l'article. Nous voulons que ce résumé soit un composant déroulant, quand on clique dessus, le résumé s'affiche sur la page d'accueil.

[https://material.angular.io/components/expansion/overview](https://material.angular.io/components/expansion/overview)

Utilisez le @Input() pour l'exercice.

**cf : diapo page 32**

## Utilisation d'un service

La première question n'était pas très satisfaisante d'un point de vue webService. En effet, si nous avions un vrai projet, il nous faudrait
créer et utiliser des webServices. Les webServices sont exposés grâce au back (que nous n'avons pas), nous allons donc le simuler.

Utilisons donc la classe getArticles qui renvoie un Observables d'article.
Un observable permet de faire appel à des API synchrones ou asynchrones. Il suffit donc de s'abonner à la resource proposée par le service, et se désabonner à la fin.

**cf : fichier src/app/shared/services/articles.service.ts**

Attention à ne pas utiliser la partie filter que nous ferons à la fin.

## Utilisation d'un routeur

Nous allons créer un composant article-detail.
Si l'on clique sur le titre de l'article dans l'accueil, cela doit nous rediriger vers une page qui contient le nom de l'article, l'année, le résumé et l'url.
Nous utiliserons pour cela un routeur, avec un passage de paramètre sur l'id de l'article. Ainsi, il restera à rechercher parmis le tableau de TOUS les articles, l'élèment à l'id correspondant.

**cf : diapo page 16**

**cf : diapo page 17**

## Création d'un filter

Le but du filtre est de filtrer la liste des articles affichés en fonction de la valeur d'un input.
Pour cela il faut changer le service d'affichage des articles. En effet, le filtre va s'abonner au changement de l'input et changer la liste d'affichage du service.
Ainsi, tous les composants qui utiliserons la fonction getArticles() du services utiliserons le filtre.

Pour cela un peu de documentation sur le filtre javascript : [https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter)

**cf : fichier src/app/shared/services/articles.service.ts**


*src/app/components/header/header.component.ts*
```ts
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ArticlesService } from 'src/app/shared/services/articles.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  private filtersubscription: Subscription;
  constructor(
    private formBuilder: FormBuilder, private articlesService: ArticlesService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: ''
    });
    this.filtersubscription = this.form.get('search').valueChanges.subscribe(val => {
      this.articlesService.filterArticles(val);
    });
  }

  ngOnDestroy(): void {
    this.filtersubscription.unsubscribe();
  }

}
```

